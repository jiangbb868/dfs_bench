# dfs_bench

#### 介绍
用于分布式文件系统的基准测试套件。

#### 软件架构
软件架构说明


#### 安装教程

编译前准备

yum install gcc -y

 

编译调试版本

gcc *.h *.c -pthread -g -o dfs_bench

编译发布版本

gcc *.h *.c -pthread -o dfs_bench

#### 使用说明

-t    线程数，必须为数字。

-op   操作，read|write|delete，多个操作用','分隔，中间不允许出现空格

-c    数量，必须为数字，passive被动模式忽略此选项。

-bs    每个文件的字节数，passive被动模式忽略此选项。

-l     日志级别, info|warn|debug|error|trace，级别依次增大

-m     运行模式，normal|active|passive， 各个模式的含义如下：

​       normal：在本节点写文件、读文件。

​      passive：被动模式，接收其他节点的请求消息，读文件、删除文件。

​      active：主动模式，在本节点写文件，通知passive节点读取或删除文件。

-dir   文件读取和写入的目录，passive被动模式忽略次选项。

-h     远程节点的ip地址，active主动模式下必须。

-n     主动/被动模式时，创建连接的数量。

-ft    文件类型，file|dir。



查看帮助

./dfs_bench help

 

正常模式启动

./dfs_bench -t 2 -dir /u01/file -op write -c 100 -bs 1024 -l error

./dfs_bench -t 2 -dir /u01/file -op read -l error

./dfs_bench -t 2 -dir /u01/file -op delete -l error

 

被动模式启动

./dfs_bench -t 1 -op read -m passive -l error

./dfs_bench -t 1 -op delete -m passive -l error

 

./dfs_bench -t 1 -op read -m passive -ft dir

./dfs_bench -t 1 -op delete -m passive -ft dir

 

主动模式启动

./dfs_bench -t 2 -dir /u01/file -op write -m active -n 10 -c 100 -bs 1024 -h 127.0.0.1 -l error 

./dfs_bench -t 1 -dir /u01/file -op write -m active -n 10 -c 5 -ft dir -bs 1024 -h 127.0.0.1 -l debug



示例：

5个线程进行写文件操作，文件目录/data，每个文件1024字节，总计102400个文件

./dfs_bench -t 5 -dir /data -op write -c 102400 -bs 1024 

5个线程进行文件读取操作，文件目录/data

./dfs_bench -t 5 -dir /data -op read 



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
